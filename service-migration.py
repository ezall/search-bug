import logging
import os
import re
import time
import boto3
from hashlib import md5
from pathlib import Path
from typing import Dict, List



    md5_hash=(str, Field('', description='MD5 hash of migration script')), 

    def validate_file(self, coordinate: str, file: Path) -> bool:
        if not self.validate_filename_re.search(file.name):
            LOGGER.error(
                f'[ERROR]: Migration filename {file.name} does not match regex format {MIGRATION_FILENAME_EXPR}')
            return False

        migrations_dict = self._get_migrations_dict_validate_cache(coordinate)
        filename_str = file.name
        if filename_str in migrations_dict:
            file_hashsum = self._calculate_file_checksum(file)
            file_hashsum_db = migrations_dict[filename_str].md5_hash
            if file_hashsum != file_hashsum_db:
                raise MigrationServiceException(f"Migration file {filename_str} MD5 hash sum mismatch with DB")
        return True